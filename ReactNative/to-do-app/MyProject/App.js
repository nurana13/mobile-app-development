import React, { useState } from "react";
import {
    StyleSheet,
    Text,
    View,
    Button,
    TextInput,
    ScrollView,
    FlatList,
    Keyboard,
    TouchableOpacity,
} from "react-native";

export default function App() {
    const [newGoalText, setNewGoalText] = useState("");

    const [goals, setGoals] = useState([]);

    const inputChangeHandler = (text) => setNewGoalText(text);

    const addClickHandler = () => {
        if (newGoalText.trim().length) {
            setGoals((goals) => [
                { uid: Math.random().toString(), text: newGoalText },
                ...goals,
            ]);
            setNewGoalText("");
            Keyboard.dismiss();
        }
    };

    const deleteGoal = (uid) => {
        setGoals((goals) => goals.filter((goal) => goal.uid !== uid));
    };

    return (
        <View style={styles.container}>
            {/* Form part */}
            <View style={styles.form}>
                <TextInput
                    value={newGoalText}
                    onChangeText={inputChangeHandler}
                    style={styles.input}
                    placeholder="Write goal text"
                />
                <Button title="ADD" onPress={addClickHandler} />
            </View>

            {/* List part */}

            <View style={styles.list}>
                <FlatList
                    data={goals}
                    renderItem={({ item }) => (
                        <TouchableOpacity
                            activeOpacity={0.6}
                            onLongPress={() => deleteGoal(item.uid)}
                        >
                            <View style={styles.goal}>
                                <Text>{item.text}</Text>
                            </View>
                        </TouchableOpacity>
                    )}
                    keyExtractor={({ uid }) => uid}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 40,
        paddingHorizontal: 30,
        backgroundColor: "lightgrey",
    },
    form: {
        flexDirection: "row",
        justifyContent: "space-between",
    },
    input: {
        backgroundColor: "white",
        borderRadius: 40,
        width: "80%",
        paddingHorizontal: 15,
    },
    list: {
        marginTop: 40,
    },
    goal: {
        backgroundColor: "white",
        padding: 15,
        borderRadius: 8,
        marginBottom: 10,
    },
});


