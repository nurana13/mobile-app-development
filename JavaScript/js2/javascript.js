let num1 = prompt("enter first number please");
while (isNaN(num1) || num1 === "") {
    num1 = prompt("Please enter a number correctly");
}
let num2 = prompt("enter second number please");
while (isNaN(num2) || num2 === "") {
    num2 = prompt("Please enter a number correctly");
}
let opp = prompt("enter an operation please");
while (opp != "/" && opp != "*" && opp != "+" && opp != "-") {
    opp = prompt("please enter an operation correctly");
}
function calculate(num1, num2, opp) {
    let result;
    if (opp == "+") result =parseInt(num1)  + parseInt(num2);
    else if (opp == "-") result = num1 - num2;
    else if (opp == "*") result = num1 * num2;
    else result = num1 / num2;
    return result;
}
console.log(`${num1} ${opp} ${num2} = ${calculate(num1, num2, opp)}`);