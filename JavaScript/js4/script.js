function filterBy(arr, type) {
    let arr2 = [];
    for (let i = 0; i < arr.length; i++) {
        arr2.push(arr[i]);
    }
    arr2.map((el, i) => {
        if (typeof el === type || (el === null && type === "null"))
            arr2.splice(i, 1);
    });
    return arr2;
}
let arr = ["hello", 12, null, "23", "clicked", 23, undefined];
console.log(filterBy(arr, "null"));