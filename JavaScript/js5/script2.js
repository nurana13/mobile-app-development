function showHide(e, name) {
    let tabsText = document.getElementsByClassName("tabs-content-text");
    for (let i = 0; i < tabsText.length; i++) {
        tabsText[i].style.display = "none";
    }
    let tabsTitle = document.getElementsByClassName("tabs-title");
    for (let i = 0; i < tabsTitle.length; i++) {
        tabsTitle[i].className = tabsTitle[i].className.replace("active", "");
    }
    document.getElementById(name).style.display = "block";
    e.currentTarget.className += " active";
}