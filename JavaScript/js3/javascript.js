function CreateNewUser(name, surname) {
    this.surname = surname;
    this.name = name;
    return (newUser = {
        firstName: this.name,
        lastName: this.surname,
        getLogin: (firstName, lastName) =>
            (firstName.substring(0, 1) + lastName).toLowerCase()
    });
}
let user = new CreateNewUser();

function setFirstName() {
    let setName = prompt("name please");
    let f = false;
    let setNameArr = [...setName];
    for (let i = 0; i < setNameArr.length; i++) {
        if (parseInt(setNameArr[i]) >= 0 && parseInt(setNameArr[i]) <= 9) f = true;
    }
    while (!isNaN(setName) || f === true) {
        f = false;
        setName = prompt("Correct name please");
        setNameArr = [...setName];
        for (let i = 0; i < setNameArr.length; i++) {
            if (parseInt(setNameArr[i]) >= 0 && parseInt(setNameArr[i]) <= 9)
                f = true;
        }
    }
    return setName;
}

function setSurName() {
    let setSurName = prompt("surname please");
    let f = false;
    let setSurNameArr = [...setSurName];
    for (let i = 0; i < setSurNameArr.length; i++) {
        if (parseInt(setSurNameArr[i]) >= 0 && parseInt(setSurNameArr[i]) <= 9)
            f = true;
    }
    while (!isNaN(setSurName) || f === true) {
        f = false;
        setSurName = prompt("Correct surname please");
        setSurNameArr = [...setSurName];
        for (let i = 0; i < setSurNameArr.length; i++) {
            if (parseInt(setSurNameArr[i]) >= 0 && parseInt(setSurNameArr[i]) <= 9)
                f = true;
        }
    }
    return setSurName;
}
console.log(user.getLogin(setFirstName(), setSurName()))