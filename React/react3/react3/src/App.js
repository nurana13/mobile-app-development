import React from 'react';
import NoteMenager from "./components/NoteMenager/NoteMenager"
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'
import {BrowserRouter,Route} from "react-router-dom"

function App() {
  return (

    <BrowserRouter>
    <Route path="/" exact render={()=>
      <NoteMenager
      />}/>
    </BrowserRouter>

  );
}
export default App;
