import React,{useContext} from 'react';
import "./NewNote.css"
import {TestContext} from "../../context"
const NewNote = (props) => {
    const [obj,setColor]=useContext(TestContext)
const toogleNewNoteModal=()=>{

    setColor({...obj,toogle:true})

}


    return (

            <button className="NewNoteBtn" onClick={toogleNewNoteModal}>Create</button>

    );
}

export default NewNote;
