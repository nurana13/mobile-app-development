import React from 'react'
import "./AlertMessage.css"

function alertMessage(props) {
    return (
<div className={props.classes.join(" ")} role="alert">
    <i className={props.check.join(" ")}></i>
  {props.message} 
</div>
        
    )
}


export default alertMessage

