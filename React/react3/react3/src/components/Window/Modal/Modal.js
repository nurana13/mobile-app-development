import React,{useContext,useState} from 'react';
import "./Modal.css"

import uuid from "uuid"
  import {TestContext} from "../../../context"





function Modal(props){

  const [obj,setObj]=useContext(TestContext)
  const [singleNote,upNote]=useState({
    title:"",
    body:""
  })



  const finishAddNote = (e)=>{
    e.preventDefault()
    singleNote.status="archived"

    setObj({...obj,toogle:false,data:[...obj.data,singleNote],cpyData:[...obj.cpyData,singleNote]})


  }
  const getInputValue=(e)=>{
    e.preventDefault()

    upNote({...singleNote,[e.target.name]:e.target.value,id:uuid(),date:new Date()})



  }


  const closeModal=()=>{
    setObj({...obj,toogle:false})
  }

  return (
        <div className="Modal">
        <p className="text-right text-danger" onClick={closeModal}><i className="fa fa-close"></i></p>
          <h2 className="text"> Fill Data</h2>
          <form>
  <div className="form-group">
    <input type="text" className="form-control" placeholder="Title" name="title"  onChange={getInputValue}/>

  </div>
  <div className="form-group">
    <textarea  className="form-control"  placeholder="Note Text" name="body"  onChange={getInputValue}/>
  </div>
            <label>Color:</label>
            <button className="color1" type="submit" value={''}></button>
            <button className="color2" type="submit" value={''}></button>
            <button className="color3" type="submit" value={''}></button>
            <button className="color4" type="submit" value={''}></button>

            <div className="modalButtons">
           <button  className="btn btn-primary"
                    onClick={finishAddNote} >Create</button>

            </div>

        </form>



        </div>
    );
}

export default Modal;
