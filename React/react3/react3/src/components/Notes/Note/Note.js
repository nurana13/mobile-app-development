import React ,{useContext}from 'react';
import "./Note.css"
import {TestContext} from "../../../context"


const Note = (props) => {
  let data=require("../../../data.json")
  const [obj,setObj]=useContext(TestContext)
 
 
  
 
const remove=(id)=>{
     const filterNote=data.filter(note=>note.id!==id)

     setObj({...obj,data:filterNote,cpyData:filterNote})


}
  
    return (
        
        <div className="Note col-lg-3 col-md-4 col-sm-6">

        <div className="card bg-warning">
  <div className="card-body">
    <h2 className="card-title">{props.title}</h2>
    
    <p className="card-text">{props.body}</p>
    <h3 className="card-subtitle mb-2 text-muted">{props.author_name}</h3>
   
    <button className="btn btn-danger" onClick={()=>remove(props.id)}>Edit</button>
      <button className="btn btn-danger" onClick={()=>remove(props.id)}>Archive</button>
      <button className="btn btn-danger" onClick={()=>remove(props.id)}>Delete</button>




  </div>
</div>
            
           
        </div>
        
    );
}

export default Note;
