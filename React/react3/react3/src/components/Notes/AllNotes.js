import React from 'react';
import Note from "./Note/Note"
import "./AllNotes.css"

const Notes = (props) => {
let archivedNotes=props.notes.filter(note=>note.status==="archived")
    return (
        <div className="container">
    <div className="row">
    {archivedNotes.map(note=>(
        <Note
        remove={props.removeNote}
        key={note.id}
        removeNotes={props.removeNote}
        title={note.title}
        body={note.body}
        id={note.id}
        removeNotes={props.removeNote}
        />
))}
    </div>
        </div>
    );
};
export default Notes;
