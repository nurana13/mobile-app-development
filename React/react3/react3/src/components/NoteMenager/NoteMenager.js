import React,{useContext} from 'react';
import NewNote from "../NewNote/NewNote"
import Archive from "../Archive/Archive"
import Notes from "../Notes/AllNotes"
import Modal from "../Window/Modal/Modal"
import {TestContext} from "../../context"
import "./NoteMenager.css"
import {Link} from "react-router-dom";

const NoteMenager = (props) => {
  const [obj]=useContext(TestContext)
  let modal=null
  if(obj.toogle){
    modal= <Modal/>
  }
  return (
        <div>
       {modal}

        <div className="ButtonsGroup">
            <div>
                <nav className="navbar navbar-expand-lg navbar-light Navbar">
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav">
                            <li className="nav-item active">
                                <Link className="nav-link" to="/">Actual</Link>
                            </li>
                            <li className="nav-item">
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <NewNote />
            <Archive />
            </div>
            <Notes  notes={obj.data}  />


        </div>
    );
}

export default NoteMenager;
