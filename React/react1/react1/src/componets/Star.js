import React, {Component} from 'react';

class Star extends Component {
    state = {
        id:this.props.id,
        isLiked: null
    };
    componentDidMount() {
        this.setStar(this.props.product.liked)
    }
    setStar = (status) => {
        this.setState({isLiked: status})
    }

    onLikedClick =(product)=> {
        console.log(product);
        let storage = JSON.parse(localStorage.getItem('liked'));
        const findProduct = storage.find(item => item.id === product.id);
        const indexProduct = storage.indexOf(findProduct);
        if(findProduct) {
            storage.splice(indexProduct, 1);
            return localStorage.setItem('liked', JSON.stringify(storage));
        }
        product.liked = true;
        storage.push(product);
        localStorage.setItem('liked', JSON.stringify(storage));
    };

    render() {
        const {product} = this.props;
        const isLiked = this.state.isLiked;
        return (
            <div >
                <i id={product.id} onClick={()=> {
                    this.onLikedClick(product);
                    this.setStar(!isLiked)
                }} className={(isLiked) ? 'fa fa-star' : 'fa fa-star-o'} aria-hidden="true"> </i>
            </div>
        );
    }
}
export default Star;
