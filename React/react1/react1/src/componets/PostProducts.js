import React, {Component} from 'react';
import axios from 'axios';
import Product from './Product';

class PostProducts extends Component {
    state = {
        products:[],
        likedProducts: [],
    };

    componentDidMount() {
        axios.get('./data.json')
            .then(response => {
                this.setState({products:response.data});
                if(localStorage.liked) {
                    this.setState({likedProducts: JSON.parse(localStorage.getItem('liked'))});
                } else {
                    localStorage.setItem('liked', JSON.stringify([{}, {}]));
                }
            })
            .catch(error => {
                console.log(error)
            })

    }
    render() {
        const {products, likedProducts} = this.state;
        return (
            <main>
                <h1>Best Mail-Order Pies</h1>
                { products.length  && likedProducts.length ?
                    products.map((product, i) => {
                        const findProduct = likedProducts.find(item => item.id === product.id);
                        return <Product key={i} productItem={findProduct ? findProduct: product}/>
                    }) : null
                }
            </main>

        );
    }
}

export default PostProducts;