import React, {Component} from 'react';
import PropTypes from "prop-types";


class Button extends Component {
    render() {
        const {color, text, doSomething} = this.props;
        return (
            <div className="d-inline">
                <button style={{background:color}}
                        className="btn m-2" onClick={doSomething}>{text}</button>
            </div>
        );
    }
}
Button.propTypes = {
    text: PropTypes.string,
    color: PropTypes.string,
    doSomething: PropTypes.func
};
export default Button;

