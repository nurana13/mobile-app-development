import React, {Component} from 'react';
import Button from "./Button";
import Star from "./Star";
import Modal from "./Modal";

class Product extends Component {
    state = {
        activeModal: '',
        showModal: false,
        isBought: null
    };
    toggleModal = (modalName) => {
        this.setState({
            showModal: ! this.state.showModal,
            activeModal: modalName
        })
    };
    componentDidMount() {
        this.setBuy(this.props.productItem.bought)
    };
    setBuy = (status) => {
        this.setState({isBought: status})
    };
    onBuyClick =(productItem)=> {
        productItem.bought = false;
        let storage2 = JSON.parse(localStorage.getItem('bought'));
        const findProduct2 = storage2.find(item => item.id === productItem.id);
        const indexProduct2 = storage2.indexOf(findProduct2);
        if(findProduct2) {
            storage2.splice(indexProduct2, 1);
            return localStorage.setItem('bought', JSON.stringify(storage2));
        } else {
            productItem.bought = true;
            storage2.push(productItem);
            localStorage.setItem('bought', JSON.stringify(storage2));
        }
    };

    render() {
        const modals = [
            {
                name: 'modal1',
                header: 'Do you want to add the product into the cart?',
                text: 'Once you added this product, you won`t be able to undo this action. Are you sure you want to add it?',
                closeButton:'none',
                actions:'ADD TO CART'
            },
            {
                name: 'modal2',
                header: 'Do you want to remove the product from the cart?',
                text: 'Once you removed this product, you won`t be able to undo this action. Are you sure you want to remove it?',
                closeButton:'block',
                actions:'REMOVE FROM CART'
            }
        ];
        const isBought = this.state.isBought;
        const {productItem} = this.props;
        const {showModal} = this.state;
        const activeModalObject = modals.find(m => m.name === this.state.activeModal);
        const activeModal = <Modal {...activeModalObject} toggleModal={() => this.toggleModal(activeModalObject.name)} />;
        return (
            <div className="d-inline-block" id={productItem.id}>
                <div className="card my-card">
                    <div className="card-body">
                        <img className='img' src={productItem.url} alt=""/>
                        <h5 className="card-title">{productItem.title}</h5>
                        <p className="card-text">{productItem.color}</p>
                        <p className="card-text">{productItem.price}</p>
                        <div>
                                <Button doSomething={() => {
                                    if (productItem.bought === true){
                                        this.toggleModal('modal2');
                                        this.onBuyClick(productItem);
                                        this.setBuy(!isBought);
                                    } else {
                                        this.toggleModal('modal1');
                                        this.onBuyClick(productItem);
                                        this.setBuy(!isBought);
                                    }
                                }}
                                        color={(productItem.bought === true) ? 'lightblue' : 'green'}
                                        text={(productItem.bought === true) ? 'REMOVE FROM CART' : 'ADD TO CART'}/>
                            {showModal && activeModal}
                            <div className='starPosition'>
                                <Star product={productItem}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Product;