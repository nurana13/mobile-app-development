import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class Tollbar extends Component {
    render() {
        return (
                <div>
                    <nav className="navbar navbar-light" style={{backgroundColor: "#e3f2fd"}}>
                        <ul className="navbar-nav" style={{display:"inline-block"}}>
                            <li className="nav-item" style={{display:"inline-block", margin:'10px'}}><Link to="/">Home</Link></li>
                            <li className="nav-item" style={{display:"inline-block", margin:'10px'}}><Link to="/Cart">Cart</Link></li>
                            <li className="nav-item" style={{display:"inline-block", margin:'10px'}}><Link to="/favourites">Favourites</Link></li>
                        </ul>
                    </nav>
                </div>
        );
    }
}

export default Tollbar;