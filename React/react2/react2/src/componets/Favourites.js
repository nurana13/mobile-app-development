import React, {Component} from 'react';
import Product from "./Product";

class Favourites extends Component {

    render() {
        const starItems =  JSON.parse(localStorage.getItem('liked'));
        return (
            <div>
                <h1>Favourites</h1>
                { starItems.length ?
                    starItems.map((product, i) => {
                        return <Product key={i} productItem={product}/>
                    }) : null
                }
            </div>
        );
    }
}

export default Favourites;